package kz.mathcode.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.service.Service;

public class StudentController extends AbstractController<Student> {
    public StudentController(Service<Student> service, ObjectMapper objectMapper) {
        super(service, objectMapper, Student.class);
    }
}
