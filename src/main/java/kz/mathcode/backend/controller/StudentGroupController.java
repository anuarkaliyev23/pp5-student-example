package kz.mathcode.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.mathcode.backend.model.StudentGroup;
import kz.mathcode.backend.service.Service;

public class StudentGroupController extends AbstractController<StudentGroup> {
    public StudentGroupController(Service<StudentGroup> service, ObjectMapper objectMapper) {
        super(service, objectMapper, StudentGroup.class);
    }
}
