package kz.mathcode.backend.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

@DatabaseTable
public class StudentGroup implements Model{
    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String specialtyName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public StudentGroup() {
    }

    public StudentGroup(int id, String name, String specialtyName) {
        this.id = id;
        this.name = name;
        this.specialtyName = specialtyName;
    }

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_SPECIALTY_NAME = "specialty_name";
    public static final String FIELD_STUDENTS = "students";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentGroup that = (StudentGroup) o;
        return id == that.id && Objects.equals(name, that.name) && Objects.equals(specialtyName, that.specialtyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, specialtyName);
    }

    @Override
    public String toString() {
        return "StudentGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", specialty_name='" + specialtyName + '\'' +
                '}';
    }
}
