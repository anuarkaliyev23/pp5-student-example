package kz.mathcode.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import io.javalin.Javalin;
import kz.mathcode.backend.configuration.DatabaseConfiguration;
import kz.mathcode.backend.configuration.JdbcDatabaseConfiguration;
import kz.mathcode.backend.controller.Controller;
import kz.mathcode.backend.controller.StudentController;
import kz.mathcode.backend.controller.StudentGroupController;
import kz.mathcode.backend.json.deserialization.StudentDeserializer;
import kz.mathcode.backend.json.deserialization.StudentGroupDeserializer;
import kz.mathcode.backend.json.serialization.StudentGroupSerializer;
import kz.mathcode.backend.json.serialization.StudentSerializer;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.model.StudentGroup;
import kz.mathcode.backend.service.Service;
import kz.mathcode.backend.service.StudentGroupService;
import kz.mathcode.backend.service.StudentService;

import java.sql.SQLException;
import static io.javalin.apibuilder.ApiBuilder.*;

public class Main {
    public static void main(String[] args) throws SQLException {
        DatabaseConfiguration configuration = new JdbcDatabaseConfiguration("jdbc:sqlite:memory:test");

        Dao<Student, Integer> studentDao = DaoManager.createDao(configuration.connectionSource(), Student.class);
        Dao<StudentGroup, Integer> studentGroupDao = DaoManager.createDao(configuration.connectionSource(), StudentGroup.class);

        Service<Student> studentService = new StudentService(studentDao);
        StudentGroupService studentGroupService = new StudentGroupService(studentGroupDao, studentDao);

        SimpleModule simpleModule = new SimpleModule()
                .addSerializer(new StudentSerializer())
                .addSerializer(new StudentGroupSerializer(studentGroupService))
                .addDeserializer(Student.class, new StudentDeserializer(studentGroupService))
                .addDeserializer(StudentGroup.class, new StudentGroupDeserializer());

        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(simpleModule);

        Controller<Student> studentController = new StudentController(studentService, objectMapper);
        Controller<StudentGroup> studentGroupController = new StudentGroupController(studentGroupService, objectMapper);

        Javalin app = Javalin.create(javalinConfig -> {
            javalinConfig.prefer405over404 = true;
            javalinConfig.enableCorsForAllOrigins();
            javalinConfig.enableDevLogging();
        });

        app.routes(() -> {
            path("students", () -> {
                get(studentController::getAll);
                post(studentController::post);

                path(":id", () -> {
                    get(ctx -> studentController.getOne(ctx, ctx.pathParam("id", Integer.class).get()));
                    patch(ctx -> studentController.patch(ctx, ctx.pathParam("id", Integer.class).get()));
                    delete(ctx -> studentController.delete(ctx, ctx.pathParam("id", Integer.class).get()));
                });
            });

            path("groups", () -> {
                get(studentGroupController::getAll);
                post(studentGroupController::post);

                path(":id", () -> {
                    get(ctx -> studentGroupController.getOne(ctx, ctx.pathParam("id", Integer.class).get()));
                    patch(ctx -> studentGroupController.patch(ctx, ctx.pathParam("id", Integer.class).get()));
                    delete(ctx -> studentGroupController.delete(ctx, ctx.pathParam("id", Integer.class).get()));
                });
            });
        });

        app.start(7000);
    }
}
