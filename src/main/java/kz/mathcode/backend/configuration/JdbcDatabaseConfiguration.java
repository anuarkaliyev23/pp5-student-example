package kz.mathcode.backend.configuration;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import kz.mathcode.backend.exception.ApplicationException;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.model.StudentGroup;

import java.sql.SQLException;

public class JdbcDatabaseConfiguration implements DatabaseConfiguration{
    private final ConnectionSource connectionSource;

    public JdbcDatabaseConfiguration(String jdbcConnectionString) {
        try {
            connectionSource = new JdbcConnectionSource(jdbcConnectionString);

            TableUtils.createTableIfNotExists(connectionSource, Student.class);
            TableUtils.createTableIfNotExists(connectionSource, StudentGroup.class);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            throw new ApplicationException("Can't initialize database connection", throwable);
        }
    }

    @Override
    public ConnectionSource connectionSource() {
        return connectionSource;
    }
}
