package kz.mathcode.backend.service;

import com.j256.ormlite.dao.Dao;
import kz.mathcode.backend.model.Student;

public class StudentService extends AbstractService<Student> {
    public StudentService(Dao<Student, Integer> dao) {
        super(dao);
    }
}
