package kz.mathcode.backend.service;

import com.j256.ormlite.dao.Dao;
import kz.mathcode.backend.exception.ApplicationException;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.model.StudentGroup;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class StudentGroupService extends AbstractService<StudentGroup> {
    private final Dao<Student, Integer> studentDao;

    public StudentGroupService(Dao<StudentGroup, Integer> dao, Dao<Student, Integer> studentDao) {
        super(dao);
        this.studentDao = studentDao;
    }

    public List<Student> findStudents(int studentGroupId) {
        try {
            List<Student> students = studentDao.queryForAll();
            return students.stream().filter(it -> it.getStudentGroup().getId() == studentGroupId).collect(Collectors.toList());
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            throw new ApplicationException("SQL Exception occurred", throwable);
        }
    }
}
