package kz.mathcode.backend.json.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.model.StudentGroup;
import kz.mathcode.backend.service.StudentGroupService;

import java.io.IOException;

public class StudentGroupSerializer extends StdSerializer<StudentGroup> {
    private final StudentGroupService service;

    public StudentGroupSerializer(StudentGroupService studentGroupService) {
        super(StudentGroup.class);
        service = studentGroupService;
    }

    @Override
    public void serialize(StudentGroup studentGroup, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField(StudentGroup.FIELD_ID, studentGroup.getId());
        jsonGenerator.writeStringField(StudentGroup.FIELD_NAME, studentGroup.getName());
        jsonGenerator.writeStringField(StudentGroup.FIELD_SPECIALTY_NAME, studentGroup.getSpecialtyName());
        jsonGenerator.writeArrayFieldStart(StudentGroup.FIELD_STUDENTS);
        for (Student student : service.findStudents(studentGroup.getId())) {
            jsonGenerator.writeObject(student);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
