package kz.mathcode.backend.json.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import kz.mathcode.backend.model.Student;

import java.io.IOException;

public class StudentSerializer extends StdSerializer<Student> {

    public StudentSerializer() {
        super(Student.class);
    }

    @Override
    public void serialize(Student student, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField(Student.FIELD_ID, student.getId());
            jsonGenerator.writeStringField(Student.FIELD_FIRST_NAME, student.getFirstName());
            jsonGenerator.writeStringField(Student.FIELD_LAST_NAME, student.getLastName());
            jsonGenerator.writeStringField(Student.FIELD_EMAIL, student.getEmail());
            jsonGenerator.writeStringField(Student.FIELD_PHONE, student.getPhone());
            jsonGenerator.writeStringField(Student.FIELD_STUDENT_GROUP, student.getStudentGroup().getName());
            jsonGenerator.writeEndObject();
    }
}
