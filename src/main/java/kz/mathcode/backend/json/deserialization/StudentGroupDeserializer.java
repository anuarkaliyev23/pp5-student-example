package kz.mathcode.backend.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.mathcode.backend.model.StudentGroup;

import java.io.IOException;

public class StudentGroupDeserializer extends StdDeserializer<StudentGroup> {
    public StudentGroupDeserializer() {
        super(StudentGroup.class);
    }

    @Override
    public StudentGroup deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        int id = root.get(StudentGroup.FIELD_ID).asInt();
        String name = root.get(StudentGroup.FIELD_NAME).asText();
        String specialty = root.get(StudentGroup.FIELD_SPECIALTY_NAME).asText();
        return new StudentGroup(id, name, specialty);
    }
}
