package kz.mathcode.backend.json.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import kz.mathcode.backend.exception.ApplicationException;
import kz.mathcode.backend.model.Student;
import kz.mathcode.backend.model.StudentGroup;
import kz.mathcode.backend.service.Service;

import java.io.IOException;

public class StudentDeserializer extends StdDeserializer<Student> {
    private final Service<StudentGroup> studentGroupService;

    public StudentDeserializer(Service<StudentGroup> studentGroupService) {
        super(Student.class);
        this.studentGroupService = studentGroupService;
    }

    @Override
    public Student deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        int id = root.get(Student.FIELD_ID).asInt();
        String firstName = root.get(Student.FIELD_FIRST_NAME).asText();
        String lastName = root.get(Student.FIELD_LAST_NAME).asText();
        String phone = root.get(Student.FIELD_PHONE).asText();
        String email = root.get(Student.FIELD_EMAIL).asText();
        int groupId = root.get(Student.FIELD_STUDENT_GROUP).asInt();
        StudentGroup group = studentGroupService.findById(groupId);
        return new Student(id, firstName, lastName, phone, email, group);
    }
}
